from selenium.webdriver.common.by import By

from pages.base_page import BasePage
from utils.config_loader import read_config_from_current_env
from utils.web_driver_actions import locator_by


class FormularioDeCotizacion(BasePage):

    __SPINNER = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//div[@class='p-progress-spinner']"
    })

    __FORMULARIO_DE_COTIZACION = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//p[contains(text(),'Formulario de cotización')]"
    })

    __INPUT_LOCALIDAD = locator_by({
        'BY': By.ID,
        'LOCATOR': 'formulario-input-localidad'
    })

    __SELECCIONA_BOTON_CONTINUAR = locator_by({
        'BY': By.ID,
        'LOCATOR': 'formulario-button-continuar'
    })

    __SELECCIONA_PRIMER_LOCALIDAD_LISTA = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//li[contains(@class,'p-autocomplete-item')][1]"
    })

    __DROPDOWN_VIGENCIA_CUATRIMESTRAL_UNA_CUOTA = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//li[@aria-label='Cuatrimestral 1  cuota']"
    })

    __DROPDOWN_VIGENCIA = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//span[contains(text(),'Mensual')]"
    })

    def __init__(self):
        super().__init__()

    def go(self):
        marketing_url = read_config_from_current_env('portal_EKO_url')
        self.navigate_to(url=marketing_url)

    def is_displayed(self):
        try:
            self.wait_for_element(self.__FORMULARIO_DE_COTIZACION)
            return True
        except:
            return False

    def select_boton_continuar(self):
        self.click_element(self.__SELECCIONA_BOTON_CONTINUAR)

    def seleccionar_vigencia_cuatrimestral_una_cuota(self):
        self.wait_for_element(self.__DROPDOWN_VIGENCIA)
        self.click_element(self.__DROPDOWN_VIGENCIA)
        self.wait_for_element(self.__DROPDOWN_VIGENCIA_CUATRIMESTRAL_UNA_CUOTA)
        self.click_element(self.__DROPDOWN_VIGENCIA_CUATRIMESTRAL_UNA_CUOTA)

    def insertar_localidad(self, localidad):
        self.send_keys_to_element(self.__INPUT_LOCALIDAD, localidad)
        self.wait_for_element(self.__SELECCIONA_PRIMER_LOCALIDAD_LISTA)
        self.click_element(self.__SELECCIONA_PRIMER_LOCALIDAD_LISTA)
