from selenium.webdriver.common.by import By

from pages.base_page import BasePage
from utils.config_loader import read_config_from_current_env
from utils.web_driver_actions import locator_by


class MarketingPage(BasePage):

    __MENU_MARKETING = locator_by({
        'BY': By.ID,
        'LOCATOR': "menu-lateral-li-marketing"
    })

    __MENU_LATERAL_LA_TIENDA_ANDINA = locator_by({
        'BY': By.ID,
        'LOCATOR': 'menu-lateral-li-la tienda andina'
    })

    __SELECT_PRIMER_REGISTRO_GRILLA = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//tbody[@class='p-datatable-tbody']//tr[1]/td[1]"
    })

    __REGISTRO_ABIERTO = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//div[@class='p-field p-grid pl2']"
    })

    __CIERRA_REGISTRO = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//span[contains(@class,'p-dialog-header-close-icon')]"
    })

    __SPINNER = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//div[@class='p-progress-spinner']"
    })

    def __init__(self):
        super().__init__()

    def go(self):
        marketing_url = read_config_from_current_env('portal_EKO_url')
        self.navigate_to(url=marketing_url)

    def is_displayed(self):
        try:
            self.wait_for_element(self.__MENU_MARKETING)
            return True
        except:
            return False

    def select_menu_marketing(self):
        self.click_element(self.__MENU_MARKETING)

    def select_menu_lateral_la_tienda_andina(self):
        self.click_element(self.__MENU_LATERAL_LA_TIENDA_ANDINA)

    def select_primer_registro_de_la_grilla(self):
        self.wait_for_invisibility_of_element(self.__SPINNER)
        self.click_element(self.__SELECT_PRIMER_REGISTRO_GRILLA)

    def registro_is_displayed(self):
        try:
            self.wait_for_element(self.__REGISTRO_ABIERTO)
            return True
        except:
            return False

    def cierra_primer_registro(self):
        self.wait_for_element(self.__CIERRA_REGISTRO)
        self.click_element(self.__CIERRA_REGISTRO)

    def selecciona(self):
        self.select_menu_marketing()
        self.select_menu_lateral_la_tienda_andina()
        self.select_primer_registro_de_la_grilla()
        return MarketingPage

    def cierra_reg_seleccionado_y_abre_denuevo(self):
        self.cierra_primer_registro()
        self.select_menu_marketing()
        self.select_menu_lateral_la_tienda_andina()
        self.select_primer_registro_de_la_grilla()
        return MarketingPage
