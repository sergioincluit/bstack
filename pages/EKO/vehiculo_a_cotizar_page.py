from selenium.webdriver.common.by import By

from pages.base_page import BasePage
from utils.config_loader import read_config_from_current_env
from utils.web_driver_actions import locator_by


class VehiculoACotizar(BasePage):

    __SPINNER = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//div[@class='p-progress-spinner']"
    })

    __VEHICULO_A_COTIZAR_SUBTITULO = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//p[contains(text(),'Vehículo a cotizar')]"
    })

    __INPUT_ANIO = locator_by({
        'BY': By.ID,
        'LOCATOR': 'seleccion-input-ano'
    })

    __INPUT_VEHICULO = locator_by({
        'BY': By.ID,
        'LOCATOR': 'seleccion-input-busqueda'
    })

    __SELECCIONA_PRIMER_VEHICULO_LISTA = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//li[contains(@class,'p-autocomplete-item')][1]"
    })

    __SELECCIONA_BOTON_SIGUIENTE = locator_by({
        'BY': By.ID,
        'LOCATOR': 'seleccion-button-siguiente'
    })

    def __init__(self):
        super().__init__()

    def go(self):
        marketing_url = read_config_from_current_env('portal_EKO_url')
        self.navigate_to(url=marketing_url)

    def is_displayed(self):
        try:
            self.wait_for_element(self.__VEHICULO_A_COTIZAR_SUBTITULO)
            return True
        except:
            return False

    def select_boton_siguiente(self):
        self.click_element(self.__SELECCIONA_BOTON_SIGUIENTE)

    def insertar_anio(self, anio):
        self.wait_for_invisibility_of_element(self.__SPINNER)
        self.send_keys_to_element(self.__INPUT_ANIO, anio)

    def insertar_vehiculo(self, vehiculo):
        self.send_keys_to_element(self.__INPUT_VEHICULO, vehiculo)
        self.wait_for_element(self.__SELECCIONA_PRIMER_VEHICULO_LISTA)
        self.click_element(self.__SELECCIONA_PRIMER_VEHICULO_LISTA)



