from selenium.webdriver.common.by import By

from pages.base_page import BasePage
from utils.config_loader import read_config_from_current_env
from utils.web_driver_actions import locator_by


class VentaPage(BasePage):

    __MENU_Venta = locator_by({
        'BY': By.ID,
        'LOCATOR': "menu-lateral-li-venta"
    })

    __MENU_LATERAL_COTIZACIONES = locator_by({
        'BY': By.ID,
        'LOCATOR': 'menu-lateral-li-cotizaciones'
    })

    __MENU_LATERAL_VEHICULOS = locator_by({
        'BY': By.ID,
        'LOCATOR': 'menu-lateral-li-vehículos'
    })

    __SPINNER = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//div[@class='p-progress-spinner']"
    })

    def __init__(self):
        super().__init__()

    def go(self):
        marketing_url = read_config_from_current_env('portal_EKO_url')
        self.navigate_to(url=marketing_url)

    def is_displayed(self):
        try:
            self.wait_for_element(self.__MENU_Venta)
            return True
        except:
            return False

    def select_menu_venta(self):
        self.click_element(self.__MENU_Venta)

    def select_menu_lateral_cotizaciones(self):
        self.click_element(self.__MENU_LATERAL_COTIZACIONES)

    def select_menu_lateral_vehiculos(self):
        self.click_element(self.__MENU_LATERAL_VEHICULOS)
