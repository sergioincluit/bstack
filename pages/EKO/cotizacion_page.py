from selenium.webdriver.common.by import By

from pages.base_page import BasePage
from utils.config_loader import read_config_from_current_env
from utils.web_driver_actions import locator_by


class CotizacionPage(BasePage):

    __COTIZACION_SUBTITULO = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//p[contains(text(),'Cotización')]"
    })

    __BOTON_MODIFICAR_GASTOS = locator_by({
        'BY': By.ID,
        'LOCATOR': 'resultado-button-gastos'
    })

    __SPINNER = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//div[@class='p-progress-spinner']"
    })

    __RESULTADO_COTIZACION_VEHICULO = locator_by({
        'BY': By.ID,
        'LOCATOR': "resultado-div-vehiculo"
    })

    __RESULTADO_COTIZACION_SUMA = locator_by({
        'BY': By.ID,
        'LOCATOR': "resultado-div-suma"
    })

    __RESULTADO_COTIZACION_0KM = locator_by({
        'BY': By.ID,
        'LOCATOR': "resultado-div-0km"
    })

    __RESULTADO_COTIZACION_GNC = locator_by({
        'BY': By.ID,
        'LOCATOR': "resultado-div-gnc"
    })

    __RESULTADO_COTIZACION_USO = locator_by({
        'BY': By.ID,
        'LOCATOR': "resultado-div-uso"
    })

    __RESULTADO_COTIZACION_RECUPERADOR = locator_by({
        'BY': By.ID,
        'LOCATOR': "resultado-div-recuperador"
    })

    __RESULTADO_COTIZACION_AJUSTE = locator_by({
        'BY': By.ID,
        'LOCATOR': "resultado-div-ajuste"
    })

    __RESULTADO_COTIZACION_PLAN = locator_by({
        'BY': By.ID,
        'LOCATOR': "resultado-div-plan"
    })

    __BOTON_NUEVA_COTIZACION = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//span[contains(text(),'Nueva Cotización')]"
    })

    __BOTON_VOLVER = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//span[contains(text(),'Volver')]"
    })

    __BOTON_CONFIRMAR = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//span[contains(text(),'Confirmar')]"
    })

    def __init__(self):
        super().__init__()

    def is_displayed(self):
        try:
            self.wait_for_invisibility_of_element(self.__SPINNER)
            self.wait_for_element(self.__COTIZACION_SUBTITULO)
            return True
        except:
            return False

    def datos_cotizacion_is_displayed(self):
        try:
            self.wait_for_element(self.__RESULTADO_COTIZACION_VEHICULO)
            self.wait_for_element(self.__RESULTADO_COTIZACION_SUMA)
            self.wait_for_element(self.__RESULTADO_COTIZACION_0KM)
            self.wait_for_element(self.__RESULTADO_COTIZACION_GNC)
            self.wait_for_element(self.__RESULTADO_COTIZACION_USO)
            self.wait_for_element(self.__RESULTADO_COTIZACION_RECUPERADOR)
            self.wait_for_element(self.__RESULTADO_COTIZACION_AJUSTE)
            self.wait_for_element(self.__RESULTADO_COTIZACION_PLAN)
            return True
        except:
            return False

    def botones_cotizacion_is_displayed(self):
        try:
            self.wait_for_element(self.__BOTON_MODIFICAR_GASTOS)
            self.wait_for_element(self.__BOTON_CONFIRMAR)
            self.wait_for_element(self.__BOTON_VOLVER)
            self.wait_for_element(self.__BOTON_NUEVA_COTIZACION)
            return True
        except:
            return False
