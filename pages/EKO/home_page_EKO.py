from selenium.webdriver.common.by import By

from pages.base_page import BasePage
from utils.config_loader import read_config_from_current_env
from utils.web_driver_actions import locator_by


class HomePage(BasePage):

    __USER_LOGGED_LBL = locator_by({
        'BY': By.ID,
        'LOCATOR': 'topbar-usuario-link'
    })

    def __init__(self):
        super().__init__()

    def is_displayed(self):
        try:
            self.wait_for_element(self.__USER_LOGGED_LBL)
            return True
        except:
            return False
