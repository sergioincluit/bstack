from selenium.webdriver.common.by import By

from pages.base_page import BasePage
from pages.SIGMA.home_page_SIGMA import HomePageServiciosQa
from utils.config_loader import read_config_from_current_env
from utils.web_driver_actions import locator_by


class LoginPageServiciosQa(BasePage):

    __LOGIN_CONTAINER = locator_by({
        'BY': By.ID,
        'LOCATOR': "form_login"
    })

    __USER_TXT = locator_by({
        'BY': By.NAME,
        'LOCATOR': 'usuario'
    })

    __PASSWORD_TXT = locator_by({
        'BY': By.NAME,
        'LOCATOR': 'contrasena'
    })

    __SIGN_IN_BTN = locator_by({
        'BY': By.ID,
        'LOCATOR': 'acceder'
    })

    def __init__(self):
        super().__init__()

    def go(self):
        servicios_url = read_config_from_current_env('portal_SIGMA_url')
        self.navigate_to(url=servicios_url)

    def is_displayed(self):
        try:
            self.wait_for_element(self.__LOGIN_CONTAINER)
            return True
        except:
            return False

    def input_user(self, username):
        self.send_keys_to_element(self.__USER_TXT, username)

    def input_password(self, password):
        self.send_keys_to_element(self.__PASSWORD_TXT, password)

    def click_on_login_btn(self):
        self.click_element(self.__SIGN_IN_BTN)

    def login(self, username, password):
        self.input_user(username)
        self.input_password(password)
        self.click_on_login_btn()
        return HomePageServiciosQa()
