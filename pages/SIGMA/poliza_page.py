from selenium.webdriver.common.by import By

from pages.base_page import BasePage
from utils.config_loader import read_config_from_current_env
from utils.web_driver_actions import locator_by


class PolizaPage(BasePage):

    __MENU_POLIZA = locator_by({
        'BY': By.ID,
        'LOCATOR': "polizasDropdown"
    })

    __MENU_CONSULTAR_POLIZA = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//span[contains(text(),'Consultar póliza')]"
    })

    __TXT_ESCRIBIR_POLIZA_EN_BUSCADOR = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//input[@class='form-control ng-pristine ng-untouched ng-valid ng-scope ng-empty']"
    })

    __BUSCAR_POLIZA = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//button[@ng-click='vm.buscarDatos()']"
    })

    __POLIZA_ENCONTRADA = locator_by({
        'BY': By.XPATH,
        'LOCATOR': "//span[@class='referencia ng-binding']"
    })

    def __init__(self):
        super().__init__()

    def go(self):
        servicios_url = read_config_from_current_env('portal_SIGMA_url')
        self.navigate_to(url=servicios_url)

    def is_displayed(self):
        try:
            self.wait_for_element(self.__MENU_POLIZA)
            return True
        except:
            return False

    def is_displayed_poliza(self):
        try:
            self.wait_for_element(self.__POLIZA_ENCONTRADA)
            return True
        except:
            return False

    def select_menu_poliza(self):
        self.click_element(self.__MENU_POLIZA)

    def select_menu_consulta_poliza(self):
        self.click_element(self.__MENU_CONSULTAR_POLIZA)

    def type_poliza_a_buscar(self):
        self.send_keys_to_element(self.__TXT_ESCRIBIR_POLIZA_EN_BUSCADOR,"51-3005260")

    def search_numero_de_poliza(self):
        self.wait_for_element(self.__BUSCAR_POLIZA)
        self.click_element(self.__BUSCAR_POLIZA)

    def navega_y_busca_poliza(self):
        self.select_menu_poliza()
        self.select_menu_consulta_poliza()
        self.type_poliza_a_buscar()
        self.search_numero_de_poliza()
        return PolizaPage

    def numero_poliza(self):
        result = self.get_text_element(self.__POLIZA_ENCONTRADA)
        result = result[3:12]
        return result