import json
import logging
import requests


class MercantilAndinaAPI:
    def __init__(self, username, password):
        logging.info(f"Definiendo el constructor de MercantilAndinaAPI, con usuario: {username} y password: {password}")
        self.base_url = "https://servicios.qamercantilandina.com.ar/"
        self.token = self.__generar_token(username, password)

    def __generar_token(self, username, password):
        payload = {}
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
        logging.info(f"Intentando hacer post a la api:{self.base_url + 'TarjetaRC/api/v1/credenciales'}")
        response = requests.post(url=self.base_url + "TarjetaRC/api/v1/credenciales", headers=headers, data=payload,
                                 auth=(username,password))
        if response.status_code == 200:
            logging.info("Se completo la creación del token")
            return response.json()["token"]
        else:
            logging.error(f"Fallo la creación del token {response.status_code}")
            return False

    def get_token(self):
        return self.token

    def get_declaracion_jurada_por_solicitud(self, id_solicitud):
        path = f"api_ddjj_hogar/v1/ddjj/hogar/{id_solicitud}"
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': f'Bearer {self.token}'
        }
        logging.info(f"Intentando hacer post a la api:{self.base_url + path}")
        response = requests.get(url=self.base_url + path, headers=headers, data={})
        logging.info("Finalizo el proceso get a la API")
        return response

    def post_declaracion_jurada_por_solicitud(self):
        path = f"api_ddjj_hogar/v1/ddjj/hogar/"
        ddjj = open("json/declaracionjuradaporsolicitud_Post.json", 'r')
        ddjj_json = json.load(ddjj)
        payload = json.dumps(ddjj_json)
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': f'Bearer {self.token}'
        }
        logging.info(f"Intentando hacer post a la api:{self.base_url + path}")
        response = requests.post(url=self.base_url + path, headers=headers, data=payload)
        logging.info("Finalizo el proceso post a la API")
        return response

    def get_pdf_de_ddjj_por_solicitud(self, id_solicitud):
        path = f"api_ddjj_hogar/v1/ddjj/hogar/{id_solicitud}/pdf"
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': f'Bearer {self.token}'
        }
        logging.info(f"Intentando hacer post a la api:{self.base_url + path}")
        response = requests.get(url=self.base_url + path, headers=headers, data={})
        logging.info("Finalizo el proceso get a la API")
        return response
