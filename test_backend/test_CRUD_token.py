from assertpy import assert_that
from apis.mercantil_andina_api import MercantilAndinaAPI
import logging


class TestAPIToken:
    def setup_class(self):
        logging.info("Pasando credenciales para generar el token")
        self.mercantil_andina_api = MercantilAndinaAPI("PROD1", "prod1")
        logging.info("Finalizo la genereación del token")

    def test_create_onetoken(self):
        token = self.mercantil_andina_api.get_token()
        print(token)
        assert_that(token).is_not_empty()
