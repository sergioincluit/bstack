import logging
from assertpy import assert_that
from apis.mercantil_andina_api import MercantilAndinaAPI


class TestDeclaracionJuradaPorSolicitud:
    def setup_class(self):
        logging.info("Llamando al constructor de la API")
        self.mercantil_andina_api = MercantilAndinaAPI("PROD1", "prod1")

    def test_get_declaracion_jurada_por_solicitud_exitosamente(self):
        logging.info("Llamando a la función con un número de declaración jurada")
        response = self.mercantil_andina_api.get_declaracion_jurada_por_solicitud(123)
        assert_that(response.status_code).is_equal_to(200)

    def test_get_declaracion_jurada_por_solicitud_con_id_no_existente(self):
        logging.info("Llamando a la función con un número de declaración jurada")
        response = self.mercantil_andina_api.get_declaracion_jurada_por_solicitud(1233)
        assert_that(response.status_code).is_equal_to(404)

    def test_get_declaracion_jurada_por_solicitud_con_id_no_valido(self):
        logging.info("Llamando a la función con un número de declaración jurada")
        response = self.mercantil_andina_api.get_declaracion_jurada_por_solicitud("asd")
        assert_that(response.status_code).is_equal_to(400)
