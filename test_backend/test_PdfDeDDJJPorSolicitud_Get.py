import datetime
import glob
import logging
import os
from apis.mercantil_andina_api import MercantilAndinaAPI
from assertpy import assert_that
from pathlib import Path
from utils.pdf_utils import UtilsPDF


class TestPdfDeDdjjPorSolicitud:
    def setup_class(self):
        logging.info("Llamando al constructor de la API")
        self.mercantil_andina_api = MercantilAndinaAPI("PROD1", "prod1")

    def test_get_pdf_de_ddjj_por_solicitud(self):
        logging.info("Llamando a la función con un número de declaración jurada")
        response = self.mercantil_andina_api.get_pdf_de_ddjj_por_solicitud(123)
        assert_that(response.status_code).is_equal_to(200)

    def test_download_pdf_de_ddjj_por_solicitud(self):
        logging.info("Llamando a la función con un número de declaración jurada")
        response = self.mercantil_andina_api.get_pdf_de_ddjj_por_solicitud(123)
        path = Path("pdf")
        path.mkdir(parents=True, exist_ok=True)
        with open(datetime.datetime.now().strftime(f"pdf/%Y-%m-%d_%H.%M.%S.pdf"), "wb") as code:
            code.write(response.content)
            code.close()
        ruta_pdf = max(glob.iglob('pdf/*'),key=os.path.getctime)
        number_of_pages = UtilsPDF.return_number_of_pages(ruta_pdf)
        assert_that(number_of_pages).is_equal_to(1)
        page_content = UtilsPDF.return_text_of_pdf(ruta_pdf,0)
        #print(page_content)
        assert_that(page_content).contains("DECLARACION JURADA DE HOGAR")
        assert_that(page_content).contains("123")
        assert_that(page_content).contains("Seguridades Minimas")
        assert_that(page_content).contains("Seguridades Adicionales")


    def test_get_pdf_de_ddjj_por_solicitud_con_id_no_existente(self):
        logging.info("Llamando a la función con un número de declaración jurada")
        response = self.mercantil_andina_api.get_pdf_de_ddjj_por_solicitud(1233)
        assert_that(response.status_code).is_equal_to(404)

    def test_get_pdf_de_ddjj_por_solicitud_con_id_no_valido(self):
        logging.info("Llamando a la función con un número de declaración jurada")
        response = self.mercantil_andina_api.get_pdf_de_ddjj_por_solicitud("asd")
        assert_that(response.status_code).is_equal_to(400)