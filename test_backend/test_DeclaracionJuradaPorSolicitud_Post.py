import json
import logging
from pprint import pprint
from assertpy import assert_that
from apis.mercantil_andina_api import MercantilAndinaAPI


class TestDeclaracionJuradaPorSolicitud:
    def setup_class(self):
        logging.info("Llamando al constructor de la API")
        self.mercantil_andina_api = MercantilAndinaAPI("PROD1", "prod1")

    def test_post_declaracion_jurada_por_solicitud_exitosa(self):
        logging.info("Llamando a la función con un número de declaración jurada")
        response = self.mercantil_andina_api.post_declaracion_jurada_por_solicitud()
        assert_that(response.status_code).is_equal_to(201)
        pprint(response.json())
        resultado = response.json()
        assert_that(str(resultado['id'])).is_digit()
        ddjj = open("json/declaracionjuradaporsolicitud_Post.json", 'r')
        ddjj_json = json.load(ddjj)
        assert_that(resultado['solicitud']).is_equal_to(ddjj_json['solicitud'])
