import time

import allure
import pytest

from pages.SIGMA.poliza_page import PolizaPage
from utils.data_loader import DataLoader

pytestmark = [
    allure.parent_suite('EKO'),
    allure.suite('Secciones comunes'),
    allure.sub_suite('SIG127_SIG134')
]


@pytest.mark.sanity
@pytest.mark.regression
class TestSig127ySig134:
    """
    En esta clase se encuentran los Casos de prueba que verifican la siguiente User Story:
    https://lamercantil.atlassian.net/browse/SIG-127
    https://lamercantil.atlassian.net/browse/SIG-134
    """

    def setup_class(self):
        self._data = DataLoader.get_data_from_ini_file("user_data.ini")

    def test_verify_SIG_127_y_SIG_134(self, open_login_page_servicios_qa):
        """
        Test: Verificar que se pueda consultar la póliza emitida anteriormente.

        Precondiciones:
            - Póliza emitida previamente

        Steps:
            - Visualizar el portal de servicios de Mercantil Andina
            - Navegar desde Póliza a Consultar póliza
            - En el buscador ingresar un número de póliza
            - Luego hacer click sobre el botón de búsqueda

        Resultado esperado:
            - Se visualizan la póliza
        """
        login_page = open_login_page_servicios_qa
        assert login_page.is_displayed()
        home_page = login_page.login(self._data.usuario, self._data.contrasena)
        assert home_page.is_displayed()
        poliza = PolizaPage()
        poliza.navega_y_busca_poliza()
        assert poliza.is_displayed_poliza()
        assert poliza.numero_poliza() == "513005260"
