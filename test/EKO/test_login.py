import time

import allure
import pytest

from pages.EKO.login_page_EKO import LoginPage
from utils.data_loader import DataLoader

pytestmark = [
    allure.parent_suite('EKO'),
    allure.suite('Secciones comunes'),
    allure.sub_suite('Login')
]


@pytest.mark.sanity
@pytest.mark.regression
class TestLogin:
    """
    En esta clase se encuentran los Casos de prueba que verifican el login
    """

    def setup_class(self):
        self._data = DataLoader.get_data_from_ini_file("user_data.ini")

    def test_verify_home_page_is_open(self, open_login_page):
        """
        Test: Verificar elementos en la pagina de login
            El objetivo del test es verificar que se muestren todos los elementos requeridos en la pagina de login y que tengan los textos esperados

        Precondiciones:
            - El usuario tiene que estar en la pagina de login

        Steps:
            - Visualizar la pantalla de login

        Resultado esperado:
            - Se visualizan en la pagina los siguientes elementos:
                - Campo para ingresar el usuario con el placeholder "Nombre de usuario o email"
                - Campo para ingresar contraseña con el placeholder "Contraseña"
                - Boton para iniciar sesión con el texto "Iniciar sesión"
        """
        login_page: LoginPage = open_login_page
        assert login_page.is_displayed()
        home_page = login_page.login(self._data.valid_user_username, self._data.valid_user_password)
        assert home_page.is_displayed()
