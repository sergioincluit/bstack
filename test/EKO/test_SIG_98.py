import time

import allure
import pytest

from pages.EKO.login_page_EKO import LoginPage
from pages.EKO.marketing_page import MarketingPage
from utils.data_loader import DataLoader

pytestmark = [
    allure.parent_suite('EKO'),
    allure.suite('Secciones comunes'),
    allure.sub_suite('SIG98')
]


@pytest.mark.sanity
@pytest.mark.regression
class TestSig98:
    """
    En esta clase se encuentran los Casos de prueba que verifican la siguiente User Story:
    https://lamercantil.atlassian.net/browse/SIG-98
    """

    def setup_class(self):
        self._data = DataLoader.get_data_from_ini_file("user_data.ini")

    def test_verify_SIG_98(self, open_login_page):
        """
        Test: Verificar el acceso a la pantalla de La Tienda Andina.
            El objetivo del test es verificar que se muestren todos los elementos requeridos en la pagina de EKO y que tengan los textos esperados

        Precondiciones:
            - El usuario debe estar logeado dentro del portal EKO

        Steps:
            - Visualizar el portal de EKO
            - Marketing →  La Tienda Andina → Seleccionar una matrícula en particular
            - Cerrar la ventana de la matrícula abierta en el punto anterior
            - Marketing →  La Tienda Andina → Seleccionar una matrícula en particular
            - Cerrar la ventana de la matrícula abierta en el punto anterior

        Resultado esperado:
            - Se visualizan en la pagina los siguientes elementos:
                - Campo para seleccionar Marketing →  La Tienda Andina → Seleccionar una matrícula en particular
                -
        """
        login_page: LoginPage = open_login_page
        assert login_page.is_displayed()
        home_page = login_page.login(self._data.valid_user_username, self._data.valid_user_password)
        assert home_page.is_displayed()
        marketing_page = MarketingPage()
        assert marketing_page.is_displayed()
        marketing_page.selecciona()
        assert marketing_page.registro_is_displayed()
        marketing_page.cierra_reg_seleccionado_y_abre_denuevo()
        assert marketing_page.registro_is_displayed()

