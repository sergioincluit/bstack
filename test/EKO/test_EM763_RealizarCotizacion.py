import logging
import allure
import pytest
from pages.EKO.cotizacion_page import CotizacionPage
from pages.EKO.formulario_de_cotizacion_page import FormularioDeCotizacion
from pages.EKO.login_page_EKO import LoginPage
from pages.EKO.vehiculo_a_cotizar_page import VehiculoACotizar
from pages.EKO.venta_page import VentaPage
from utils.data_loader import DataLoader

pytestmark = [
    allure.parent_suite('EKO'),
    allure.suite('Secciones comunes'),
    allure.sub_suite('EM763_EKO_RealizarCotización')
]


@pytest.mark.sanity
@pytest.mark.regression
class TestEM763:
    """
    Automatizacion del siguiente test case: https://lamercantil.atlassian.net/browse/EM-763
    """

    def setup_class(self):
        self._data = DataLoader.get_data_from_ini_file("user_data.ini")

    def test_realizar_cotizacion(self, open_login_page):

        """
        DADO usuario autorizado que se encuentra logueado en EKO
        CUANDO selecciona el menú "Venta>Cotizaciones>Vehículos"
        ENTONCES se visualiza la pantalla "Vehículo a cotizar"
        CUANDO completa los campos "Año"
        Y completa el campo "Marca o modelo"
        Y seleccionar la opción "Siguiente"
        ENTONCES se visualiza la pantalla "Formulario de cotización"
        CUANDO selecciona la opción "Cuatrimestral 1 cuota" en el combo "Vigencia / plan de pago"
        Y ingresa "PIL" en el campo Localidad
        ENTONCES se despliega en el campo Localidad una lista de localidades que coincidan con la búsqueda "PIL"
        CUANDO selecciona una localidad de la lista desplegada
        Y selecciona la opción "Continuar"
        ENTONCES se visualiza la pantalla "Cotización"
        Y se visualizan los siguientes datos de la cotización:
        • Vehículo
        • Suma asegurda
        • Okm
        • GNC
        • Uso
        • Recuperador
        • Ajuste Suma
        • Plan y forma de pago
        Y se visualiza la sección "Coberturas" con todas las coberturas seleccionadas en la pantalla anterior
        Y se visualiza la opción "Modificar gastos"
        Y se visualiza la opción "Imprimir coberturas"
        Y se visualizan las opciones "Volver", "Nueva Cotización" y "Confirmar"
        """

        login_page: LoginPage = open_login_page
        assert login_page.is_displayed()
        home_page = login_page.login(self._data.username_delta, self._data.password_delta)
        assert home_page.is_displayed()
        venta_page = VentaPage()
        assert venta_page.is_displayed()
        venta_page.select_menu_venta()
        venta_page.select_menu_lateral_cotizaciones()
        venta_page.select_menu_lateral_vehiculos()
        vehiculo_a_cotizar_page = VehiculoACotizar()
        assert vehiculo_a_cotizar_page.is_displayed()
        logging.info("Ingreso año y modelo")
        vehiculo_a_cotizar_page.insertar_anio(2020)
        vehiculo_a_cotizar_page.insertar_vehiculo("FORD FOCUS")
        vehiculo_a_cotizar_page.select_boton_siguiente()
        formulario_cotizacion = FormularioDeCotizacion()
        assert formulario_cotizacion.is_displayed()
        logging.info("Ingreso localidad y vigencia")
        formulario_cotizacion.seleccionar_vigencia_cuatrimestral_una_cuota()
        formulario_cotizacion.insertar_localidad("PIL")
        formulario_cotizacion.select_boton_continuar()
        cotizacion_page = CotizacionPage()
        logging.info("Visualizando datos de cotizacion")
        assert cotizacion_page.is_displayed()
        assert cotizacion_page.datos_cotizacion_is_displayed()
        assert cotizacion_page.botones_cotizacion_is_displayed()