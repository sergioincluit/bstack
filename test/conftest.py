"""
Precondiciones que pueden ser utilizadas en todos los casos de prueba
"""
import pytest

from pages.EKO.login_page_EKO import LoginPage
from pages.SIGMA.login_page_SIGMA import LoginPageServiciosQa


@pytest.fixture()
def open_login_page(driver):
    login_page = LoginPage()
    login_page.go()
    assert login_page.is_displayed()
    return login_page


@pytest.fixture()
def open_login_page_servicios_qa(driver):
    login_page = LoginPageServiciosQa()
    login_page.go()
    assert login_page.is_displayed()
    return login_page
