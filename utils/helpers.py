import json

from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec



class Helpers:
    def obtener_parametro(parametro):
        with open('config/jsons/data.json') as portalEKOJson:
            parametros = json.load(portalEKOJson)
            return parametros[parametro]

    def isElementDisplayed(self, driver, element):
        try:
            wait = WebDriverWait(driver, 1)
            wait.until(ec.visibility_of(element))
            return element.isDisplayed
        except: return False

    def waitForElementToBeGone(self, driver, element, timeout):
        if self.isElementDisplayed(self, driver, element):
            WebDriverWait(driver, timeout).until(ec.invisibility_of_element(ec.visibility_of(element)))