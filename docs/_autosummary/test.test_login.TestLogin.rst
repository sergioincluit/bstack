TestLogin
=========

.. currentmodule:: test.test_login

.. autoclass:: TestLogin
   :members:
   :show-inheritance:
   :inherited-members:
   :special-members: __call__, __add__, __mul__

   
   
   .. rubric:: Methods

   .. autosummary::
      :nosignatures:
   
      ~TestLogin.setup_class
      ~TestLogin.test_verify_home_page_is_open
   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~TestLogin.pytestmark
   
   