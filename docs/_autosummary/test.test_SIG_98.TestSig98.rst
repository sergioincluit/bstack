TestSig98
=========

.. currentmodule:: test.test_SIG_98

.. autoclass:: TestSig98
   :members:
   :show-inheritance:
   :inherited-members:
   :special-members: __call__, __add__, __mul__

   
   
   .. rubric:: Methods

   .. autosummary::
      :nosignatures:
   
      ~TestSig98.setup_class
      ~TestSig98.test_verify_SIG_98
   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~TestSig98.pytestmark
   
   