.. MyProject documentation master file, created by
   sphinx-quickstart on Mon May 17 19:28:04 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bienvenido a la documentación de WebApp!
=========================================
.. autopackagesummary:: test
      :toctree: _autosummary
      :template: custom-module-template.rst
      :recursive:


Indices y tablas
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
